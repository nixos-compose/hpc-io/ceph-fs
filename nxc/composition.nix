{ pkgs, helpers, setup, ... }: {
  roles =
    let

      ## OSD CREATOR HELPER
      ##================================##
      # Function that translate single char into "0<char>"
      # It is used to keep the right number of values for the uuid and key
      format = (s: if ((builtins.stringLength s) < 2) then ("0" + "${toString s}") else (toString s));

      # Function that calculate the rest of the euclidian division
      modulo = a: b:
        let
          f = a: b:
            if a < b then
              a
            else
              (f (a - b) b);
        in
        f a b;

      # Function that translate an integer into its corresponding hexadecimal value  
      int2HexIn = n: mod:
        let
          digits = [ "0" "1" "2" "3" "4" "5" "6" "7" "8" "9" "a" "b" "c" "d" "e" "f" ];
          convert = n:
            if n < 16 then
              builtins.elemAt digits n
            else
              let
                q = n / 16;
                r = mod n 16;
              in
              (convert q) + builtins.elemAt digits r;
        in
        if n == 0 then "0"
        else convert n;
      ##================================##

      makeManyById = base_conf_by_id: name: count:
        let
          f = n: s:
            if n == 0 then
              s
            else
              f (n - 1) (s // { "${name}${toString (n - 1)}" = (base_conf_by_id (n - 1)); });
        in
        f count { };

      # Main immutable configuration for ceph cluster
      cfg = {
        clusterId = "066ae264-2a5d-4729-8001-6ad265f50b03";
        monA = {
          name = "a";
          hostname = "monn"; # Nom du noeud principal (si plusieurs il faut mettre le bon nom monn1 ou autre)
        };
        mds = {
          name = "meta";
          key = "AQAdyhZeIaUlARaBGRoidDAmS6Qqp546UFEf5z==";
          uuid = "3ca570f9-be10-4ed9-a88c-ce36e3cccb8d";
        };
      };

      commonConfig = import ./common-config.nix { inherit pkgs cfg; };

      # Function that will initiate the global ceph service config
      generateCephConfig = { daemonConfig }: {
        enable = true;
        global = {
          fsid = cfg.clusterId;
          monHost = "192.168.0.1"; # Temp adress that will be changed in the config by the initiateIP service
          monInitialMembers = cfg.monA.name;
        };
      } // daemonConfig;

      # OSD role : create here in order to change its key and uuid for each value
      # In order to change the number of osd use the nb_osd parameter in the setup.toml
      osd = count:
        let
          name = toString count;
          key = "AQAdyhZeIaUlARaBGRoidDAmS6Qqp546UFE${format (int2HexIn count modulo)}z==";
          uuid = "3ca570f9-be10-4ed9-a88c-ce36e3ccc${format (int2HexIn count modulo)}d";
        in
        {
          imports = [ commonConfig ];

          # Definition of the ceph system
          services.ceph = generateCephConfig {
            daemonConfig = {
              osd = {
                enable = true;
                daemons = [ name ];
              };
            };
          };
        };
    in
    {
      monn = { pkgs, ... }:
        {
          imports = [ commonConfig ];

          # Definition of the ceph system
          services.ceph = generateCephConfig {
            daemonConfig = {
              mon = {
                enable = true;
                daemons = [ cfg.monA.name ];
              };
              mgr = {
                enable = true;
                daemons = [ cfg.monA.name ];
              };
              mds = {
                enable = true;
                daemons = [ cfg.mds.name ];
              };
            };
          };
        };
      client = { pkgs, ... }: 
      {
        imports = [ commonConfig ];
      };
    } // makeManyById osd "osd" setup.params.nb_osd; # Add nb_osd different osd with unique uuid and key to the roles composition
  testScript = ''

    # Unit Test script (set nb_osd to 3 in the setup.toml) (name of monn = a) (only 1 monn (node name = monn))
    start_all()

    monn.succeed("true")
    osd1.succeed("true")
    osd2.succeed("true")
    osd3.succeed("true")

    monn.wait_for_unit("network.target")
    osd1.wait_for_unit("network.target")
    osd2.wait_for_unit("network.target")
    osd3.wait_for_unit("network.target")

    # Bootstrap ceph-mon daemon
    monn.succeed(
      "export IPADDR=$(</etc/nxc/ip_addr)",
      "sudo -u ceph ceph-authtool --create-keyring /tmp/ceph.mon.keyring --gen-key -n mon. --cap mon 'allow *'",
      "sudo -u ceph ceph-authtool --create-keyring /etc/ceph/ceph.client.admin.keyring --gen-key -n client.admin --cap mon 'allow *' --cap osd 'allow *' --cap mds 'allow *' --cap mgr 'allow *'",
      "sudo -u ceph ceph-authtool /tmp/ceph.mon.keyring --import-keyring /etc/ceph/ceph.client.admin.keyring",
      "monmaptool --create --add a $IPADDR --fsid 066ae264-2a5d-4729-8001-6ad265f50b03 /tmp/monmap",
      "sudo -u ceph ceph-mon --mkfs -i a --monmap /tmp/monmap --keyring /tmp/ceph.mon.keyring",
      "sudo -u ceph mkdir -p /var/lib/ceph/mgr/ceph-a/",
      "sudo -u ceph touch /var/lib/ceph/mon/ceph-a/done",
      "systemctl start ceph-mon-a",)

    monn.wait_for_unit("ceph-mon-a")
    monn.succeed("ceph mon enable-msgr2")
    monn.succeed("ceph config set mon auth_allow_insecure_global_id_reclaim false")

    # Can't check ceph status until a monn is up
    monn.succeed("ceph -s | grep 'mon: 1 daemons'")

    # Start the ceph-mgr daemon, it has no deps and hardly any setup
    monn.succeed(
      "ceph auth get-or-create mgr.a mon 'allow profile mgr' osd 'allow *' mds 'allow *' > /var/lib/ceph/mgr/ceph-a/keyring",
      "systemctl start ceph-mgr-a",)
    
    monn.wait_for_unit("ceph-mgr-a")
    monn.wait_until_succeeds("ceph -s | grep 'quorum a'")
    monn.wait_until_succeeds("ceph -s | grep 'mgr: a(active,'")

    # Send the admin keyring to the OSD machines
    monn.succeed("cp /etc/ceph/ceph.client.admin.keyring /tmp/shared")
    osd1.succeed("cp /tmp/shared/ceph.client.admin.keyring /etc/ceph")
    osd2.succeed("cp /tmp/shared/ceph.client.admin.keyring /etc/ceph")
    osd3.succeed("cp /tmp/shared/ceph.client.admin.keyring /etc/ceph")

    # Bootstrap OSDs
    osd1.succeed(
      "mkfs.xfs /dev/vdb",
      "mkdir -p /var/lib/ceph/osd/ceph-1",
      "mount /dev/vdb /var/lib/ceph/osd/ceph-1",
      "ceph-authtool --create-keyring /var/lib/ceph/osd/ceph-1/keyring --name osd.1 --add-key AQAdyhZeIaUlARaBGRoidDAmS6Qqp546UFE01z==",
      'echo \'{"cephx_secret": "AQAdyhZeIaUlARaBGRoidDAmS6Qqp546UFE01z=="}\' | ceph osd new 3ca570f9-be10-4ed9-a88c-ce36e3ccc01d -i -',)

    osd2.succeed(
      "mkfs.xfs /dev/vdb",
      "mkdir -p /var/lib/ceph/osd/ceph-2",
      "mount /dev/vdb /var/lib/ceph/osd/ceph-2",
      "ceph-authtool --create-keyring /var/lib/ceph/osd/ceph-2/keyring --name osd.2 --add-key AQAdyhZeIaUlARaBGRoidDAmS6Qqp546UFE02z==",
      'echo \'{"cephx_secret": "AQAdyhZeIaUlARaBGRoidDAmS6Qqp546UFE02z=="}\' | ceph osd new 3ca570f9-be10-4ed9-a88c-ce36e3ccc02d -i -',)

    osd3.succeed(
      "mkfs.xfs /dev/vdb",
      "mkdir -p /var/lib/ceph/osd/ceph-3",
      "mount /dev/vdb /var/lib/ceph/osd/ceph-3",
      "ceph-authtool --create-keyring /var/lib/ceph/osd/ceph-3/keyring --name osd.3 --add-key AQAdyhZeIaUlARaBGRoidDAmS6Qqp546UFE03z==",
      'echo \'{"cephx_secret": "AQAdyhZeIaUlARaBGRoidDAmS6Qqp546UFE03z=="}\' | ceph osd new 3ca570f9-be10-4ed9-a88c-ce36e3ccc03d -i -',)


    # Initialize the OSDs with regular filestore
    osd1.succeed(
      "ceph-osd -i 1 --mkfs --osd-uuid 3ca570f9-be10-4ed9-a88c-ce36e3ccc01d",
      "chown -R ceph:ceph /var/lib/ceph/osd",
      "systemctl start ceph-osd-1",)

    osd2.succeed(
      "ceph-osd -i 2 --mkfs --osd-uuid 3ca570f9-be10-4ed9-a88c-ce36e3ccc02d",
      "chown -R ceph:ceph /var/lib/ceph/osd",
      "systemctl start ceph-osd-2",)

    osd3.succeed(
      "ceph-osd -i 3 --mkfs --osd-uuid 3ca570f9-be10-4ed9-a88c-ce36e3ccc03d",
      "chown -R ceph:ceph /var/lib/ceph/osd",
      "systemctl start ceph-osd-3",)

    monn.wait_until_succeeds("ceph osd stat | grep -e '3 osds: 3 up[^,]*, 3 in'")
    monn.wait_until_succeeds("ceph -s | grep 'mgr: a(active,'")
    monn.wait_until_succeeds("ceph -s | grep 'HEALTH_OK'")    

    monn.succeed(
      "ceph osd pool create multi-node-test 32 32",
      "ceph osd pool ls | grep 'multi-node-test'",
      "ceph osd pool rename multi-node-test multi-node-other-test",
      "ceph osd pool ls | grep 'multi-node-other-test'",)

    monn.wait_until_succeeds("ceph -s | grep '2 pools, 33 pgs'")
    monn.succeed("ceph osd pool set multi-node-other-test size 2")
    monn.wait_until_succeeds("ceph -s | grep 'HEALTH_OK'")
    monn.wait_until_succeeds("ceph -s | grep '33 active+clean'")

    monn.fail(
      "ceph osd pool ls | grep 'multi-node-test'",
      "ceph osd pool delete multi-node-other-test multi-node-other-test --yes-i-really-really-mean-it",)

    # Shut down ceph on all machines in a very unpolite way
    monn.crash()
    osd1.crash()
    osd2.crash()
    osd3.crash()

    # Start it up
    osd1.start()
    osd2.start()
    osd3.start()
    monn.start()

    # Ensure the cluster comes back up again
    monA.succeed("ceph -s | grep 'mon: 1 daemons'")
    monA.wait_until_succeeds("ceph -s | grep 'quorum a'")
    monA.wait_until_succeeds("ceph osd stat | grep -e '3 osds: 3 up[^,]*, 3 in'")
    monA.wait_until_succeeds("ceph -s | grep 'mgr: a(active,'")
    monA.wait_until_succeeds("ceph -s | grep 'HEALTH_OK'")

  '';
}
