{ pkgs, ... }: {
  roles =
    let
      cfg = {
        clusterId = "066ae264-2a5d-4729-8001-6ad265f50b03";
        monA = {
          name = "a";
          ip = "192.168.1.1";
        };
        osd0 = {
          name = "0";
          key = "AQBCEJNa3s8nHRAANvdsr93KqzBznuIWm2gOGg==";
          uuid = "55ba2294-3e24-478f-bee0-9dca4c231dd9";
        };
        osd${id} = {
          name = "1";
          key = "AQBEEJNac00kExAAXEgy943BGyOpVH1LLlH${id}==";
          uuid = "5e97a838-85b6-43b0-8950-cb56d554d${id}";
        };
        osd2 = {
          name = "2";
          key = "AQAdyhZeIaUlARAAGRoidDAmS6Vkp546UFEf5w==";
          uuid = "ea999274-13d0-4dd5-9af9-ad25a324f72f";
        };
        mds = {
          name = "meta";
          key = "AQAdyhZeIaUlARaBGRoidDAmS6Qqp546UFEf5w==";
          uuid = "3ca570f9-be10-4ed9-a88c-ce36e3cccb8d";
        };
      };
    in
    {
      monn = { pkgs, ... }:
        {
          # virtualisation = {
          #   emptyDiskImages = [ 20480 20480 20480 ];
          #   vlans = [ 1 ];
          # };

          networking = {
            # dhcpcd.enable = false; ## PROBLEME HERE
            interfaces.eth1.ipv4.addresses = pkgs.lib.mkOverride 0 [
              { address = cfg.monA.ip; prefixLength = 24; }
            ];
          };

          environment.systemPackages = with pkgs; [
            bash
            sudo
            ceph
            xfsprogs
            libressl.nc
          ];

          boot.kernelModules = [ "xfs" ];

          services.ceph = {
            enable = true;
            global = {
              fsid = cfg.clusterId;
              monHost = cfg.monA.ip;
              monInitialMembers = cfg.monA.name;
            };
            mon = {
              enable = true;
              daemons = [ cfg.monA.name ];
            };
            mgr = {
              enable = true;
              daemons = [ cfg.monA.name ];
            };
            osd = {
              enable = true;
              daemons = [ cfg.osd0.name cfg.osd1.name cfg.osd2.name ];
            };
            ## Rajout du serveur de metadonnées
            mds = {
              enable = true;
              daemons = [ cfg.mds.name ];
            };
          };
        };
      mgrr = { pkgs, ... }:
        {
          environment.systemPackages = with pkgs; [
            bash
            sudo
            ceph
            xfsprogs
          ];
        };
      osdd = { pkgs, ... }:
        {
          environment.systemPackages = with pkgs; [
            bash
            sudo
            ceph
            xfsprogs
          ];
        };
    };
  testScript = ''
    start_all()

    monn.wait_for_unit("network.target")

    mgrr.succeed("true")
    osdd.succeed("true")
    monn.succeed("true")

    monn.succeed(
      "sudo -u ceph ceph-authtool --create-keyring /tmp/ceph.mon.keyring --gen-key -n mon. --cap mon 'allow *'",
      "sudo -u ceph ceph-authtool --create-keyring /etc/ceph/ceph.client.admin.keyring --gen-key -n client.admin --cap mon 'allow *' --cap osd 'allow *' --cap mds 'allow *' --cap mgr 'allow *'",
      "sudo -u ceph ceph-authtool /tmp/ceph.mon.keyring --import-keyring /etc/ceph/ceph.client.admin.keyring",
      "monmaptool --create --add a 192.168.1.1 --fsid 066ae264-2a5d-4729-8001-6ad265f50b03 /tmp/monmap",
      "sudo -u ceph ceph-mon --mkfs -i a --monmap /tmp/monmap --keyring /tmp/ceph.mon.keyring",
      "sudo -u ceph touch /var/lib/ceph/mon/ceph-a/done",
      "systemctl start ceph-mon-a",)

    monn.wait_for_unit("ceph-mon-a")
    monn.succeed("ceph mon enable-msgr2")
    monn.succeed("ceph config set mon auth_allow_insecure_global_id_reclaim false")

    # Can't check ceph status until a mon is up
    monn.succeed("ceph -s | grep 'mon: 1 daemons'")

    # Start the ceph-mgr daemon, after copying in the keyring
    monn.succeed(
      "sudo -u ceph mkdir -p /var/lib/ceph/mgr/ceph-a/",
      "ceph auth get-or-create mgr.a mon 'allow profile mgr' osd 'allow *' mds 'allow *' > /var/lib/ceph/mgr/ceph-a/keyring",
      "systemctl start ceph-mgr-a",)
    
    monn.wait_for_unit("ceph-mgr-a")
    monn.wait_until_succeeds("ceph -s | grep 'quorum a'")
    monn.wait_until_succeeds("ceph -s | grep 'mgr: a(active,'")

    # Bootstrap OSDs
    monn.succeed(
      "mkfs.xfs /dev/vdb",
      "mkfs.xfs /dev/vdc",
      "mkfs.xfs /dev/vdd",
      "mkdir -p /var/lib/ceph/osd/ceph-0",
      "mount /dev/vdb /var/lib/ceph/osd/ceph-0",
      "mkdir -p /var/lib/ceph/osd/ceph-1",
      "mount /dev/vdc /var/lib/ceph/osd/ceph-1",
      "mkdir -p /var/lib/ceph/osd/ceph-2",
      "mount /dev/vdd /var/lib/ceph/osd/ceph-2",
      "ceph-authtool --create-keyring /var/lib/ceph/osd/ceph-0/keyring --name osd.0 --add-key AQBCEJNa3s8nHRAANvdsr93KqzBznuIWm2gOGg==",
      "ceph-authtool --create-keyring /var/lib/ceph/osd/ceph-1/keyring --name osd.1 --add-key AQBEEJNac00kExAAXEgy943BGyOpVH1LLlHafQ==",
      "ceph-authtool --create-keyring /var/lib/ceph/osd/ceph-2/keyring --name osd.2 --add-key AQAdyhZeIaUlARAAGRoidDAmS6Vkp546UFEf5w==",
      'echo \'{"cephx_secret": "AQBCEJNa3s8nHRAANvdsr93KqzBznuIWm2gOGg=="}\' | ceph osd new 55ba2294-3e24-478f-bee0-9dca4c231dd9 -i -',
      'echo \'{"cephx_secret": "AQBEEJNac00kExAAXEgy943BGyOpVH1LLlHafQ=="}\' | ceph osd new 5e97a838-85b6-43b0-8950-cb56d554d1e5 -i -',
      'echo \'{"cephx_secret": "AQAdyhZeIaUlARAAGRoidDAmS6Vkp546UFEf5w=="}\' | ceph osd new ea999274-13d0-4dd5-9af9-ad25a324f72f -i -',)

    # Initialize the OSDs with regular filestore
    monn.succeed(
      "ceph-osd -i 0 --mkfs --osd-uuid 55ba2294-3e24-478f-bee0-9dca4c231dd9",
      "ceph-osd -i 1 --mkfs --osd-uuid 5e97a838-85b6-43b0-8950-cb56d554d1e5",
      "ceph-osd -i 2 --mkfs --osd-uuid ea999274-13d0-4dd5-9af9-ad25a324f72f",
      "chown -R ceph:ceph /var/lib/ceph/osd",
      "systemctl start ceph-osd-0",
      "systemctl start ceph-osd-1",
      "systemctl start ceph-osd-2",)

    monn.wait_until_succeeds("ceph osd stat | grep -e '3 osds: 3 up[^,]*, 3 in'")
    monn.wait_until_succeeds("ceph -s | grep 'mgr: a(active,'")
    monn.wait_until_succeeds("ceph -s | grep 'HEALTH_OK'")

    monn.succeed(
        "ceph osd pool create single-node-test 32 32",
        "ceph osd pool ls | grep 'single-node-test'",
        "ceph osd pool rename single-node-test single-node-other-test",
        "ceph osd pool ls | grep 'single-node-other-test'",)

    monn.wait_until_succeeds("ceph -s | grep '2 pools, 33 pgs'")
    monn.succeed(
        "ceph osd getcrushmap -o crush",
        "crushtool -d crush -o decrushed",
        "sed 's/step chooseleaf firstn 0 type host/step chooseleaf firstn 0 type osd/' decrushed > modcrush",
        "crushtool -c modcrush -o recrushed",
        "ceph osd setcrushmap -i recrushed",
        "ceph osd pool set single-node-other-test size 2",)

    monn.wait_until_succeeds("ceph -s | grep 'HEALTH_OK'")
    monn.wait_until_succeeds("ceph -s | grep '33 active+clean'")
    monn.fail(
        "ceph osd pool ls | grep 'multi-node-test'",
        "ceph osd pool delete single-node-other-test single-node-other-test --yes-i-really-really-mean-it",)

    # Shut down ceph by stopping ceph.target.
    monn.succeed("systemctl stop ceph.target")

    # Start it up
    monn.succeed("systemctl start ceph.target")
    monn.wait_for_unit("ceph-mon-a")
    monn.wait_for_unit("ceph-mgr-a")
    monn.wait_for_unit("ceph-osd-0")
    monn.wait_for_unit("ceph-osd-1")
    monn.wait_for_unit("ceph-osd-2")

    # Ensure the cluster comes back up again
    monn.succeed("ceph -s | grep 'mon: 1 daemons'")
    monn.wait_until_succeeds("ceph -s | grep 'quorum a'")
    monn.wait_until_succeeds("ceph osd stat | grep -e '3 osds: 3 up[^,]*, 3 in'")
    monn.wait_until_succeeds("ceph -s | grep 'mgr: a(active,'")
    monn.wait_until_succeeds("ceph -s | grep 'HEALTH_OK'")

    # Enable the dashboard and recheck health
    monn.succeed(
        "ceph mgr module enable dashboard",
        "ceph config set mgr mgr/dashboard/ssl false",
        # default is 8080 but it's better to be explicit
        "ceph config set mgr mgr/dashboard/server_port 8080",)

    monn.wait_for_open_port(8080)
    monn.wait_until_succeeds("curl -q --fail http://localhost:8080")
    monn.wait_until_succeeds("ceph -s | grep 'HEALTH_OK'")

  '';
}
