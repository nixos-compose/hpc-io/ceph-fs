{ pkgs, cfg, ... }:

{
  # Definition of the package needed bu ceph 
  environment.systemPackages = with pkgs; [
    bash
    sudo
    ceph
    xfsprogs
    libressl.nc
  ];

  boot.kernelModules = [ "xfs" ];


  ## Service made for initiating IP adress to the ceph.conf file in g5k
  # This service change the value of the immutable ceph.conf file by copying the
  # values and replacing the ip thanks to the deployment-hosts file in nxc 
  systemd.services.initiateIP = {
    wantedBy = [ "multi-user.target" ];
    requires = [ "network-online.target" ];
    after = [ "ceph.target" ];
    serviceConfig.Type = "oneshot";
    script = ''
      cp /etc/ceph/ceph.conf /etc/ceph/ceph_copy.conf
      SERVER=$( grep ${cfg.monA.hostname} /etc/nxc/deployment-hosts | ${pkgs.gawk}/bin/awk '{ print $1 }')
      # SERVER=$( cat /etc/nxc/ip_addr)
      sed -i "s/^mon host=.*/mon host=$SERVER/" /etc/ceph/ceph_copy.conf
      rm -f /etc/ceph/ceph.conf
      mv /etc/ceph/ceph_copy.conf /etc/ceph/ceph.conf

      mv /etc/ceph.client.admin.keyring /etc/ceph/ceph.client.admin.keyring
    '';
  };

  ## Networking for ceph node 
  networking = {
    firewall = {
      allowedTCPPorts = [ 6789 3300 ];
      allowedTCPPortRanges = [{ from = 6800; to = 7300; }];
    };
  };

  environment.etc."ceph.client.admin.keyring" = {
    text = ''
      [client.admin]
              key = AQARFndbOFpIABAArBobpqDZB8jmwH5QEwmP8g== 
              caps mds = "allow *"
              caps mgr = "allow *"
              caps mon = "allow *"
              caps osd = "allow *"
    '';
  };

}
