{

  makeManyById = base_conf_by_id: name: count:
    let
      f = n: s:
        if n == 0 then
          s
        else
          f (n - 1) (s // { "${name}${toString n}" = (base_conf_by_id n); });
    in
    f count { };


  format = (s: if ((builtins.stringLength s) < 2) then ("0" + "${toString s}") else (toString s));

  int2HexIn = n:
    let
      digits = "0123456789abcdef";
      convert = n:
        if n < 16 then
          digits.[n]
        else
          let
            q = n / 16;
            r = n % 16;
          in
          (convert q) + digits.[r];
    in
    if n == 0 then "0"
    else convert n;

  int2Hex = (n: format toString int2HexIn n);

  func = (x: { name = "${toString x}"; key = "AQAdyhZeIaUlARAAGRoidDAmS6Vkp546UFEf${int2Hex x}=="; uuid = "ea999274-13d0-4dd5-9af9-ad25a324f${int2Hex x}f"; });
  b = makeManyById func "osd" 3;


  generateKey = pkgs.writers.writePython3Bin "generate-key-value" { } ''
    import random

    random.seed(1)
    chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
    key_len = 39
    key = ""
    for x in range(0, key_len):
        key_char = random.choice(chars)
        key += key_char
    key += "=="
    print(key)
  '';

  generateUUID = pkgs.writers.writePython3Bin "generate-uuid-value" { } ''
    import uuid
    import random

    rd = random.Random()
    rd.seed(1)

    print(uuid.UUID(int=rd.getrandbits(128)))
  '';


  KeyValue = pkgs.runCommand "generate-key-value-output" { buildInputs = [ generateKey ]; } (
    ''
      mkdir -p $out
      ${generateKey}/bin/generate-key-value > $out/res
    ''
  );
  UUIDValue = pkgs.runCommand "generate-uuid-value-output" { buildInputs = [ generateUUID ]; } (
    ''
      mkdir -p $out
      ${generateUUID}/bin/generate-uuid-value > $out/res
    ''
  );


  systemd.services.test = {
    wantedBy = [ "multi-user.target" ];
    requires = [ "network-online.target" ];
    script = ''
      touch /etc/ceph/test_key
      cat ${KeyValue}/res >> /etc/ceph/test_key

      touch /etc/ceph/test_uuid
      cat ${UUIDValue}/res >> /etc/ceph/test_uuid
    '';
  };
}


