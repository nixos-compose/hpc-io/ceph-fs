import random

chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
key_len = 39

def main():
    key = ""
    for x in range(0, key_len):
        key_char = random.choice(chars)
        key += key_char
    key += "=="
    print(key)

if __name__ == "__main__":
    main()